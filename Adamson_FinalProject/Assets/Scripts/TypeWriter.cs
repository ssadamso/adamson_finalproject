﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeWriter : MonoBehaviour
{
    public string display;
    public Text textbox;
    public float textSpeed;

    Coroutine typewriteRoutine;

    // Start is called before the first frame update
    void Start()
    {
        typewriteRoutine = StartCoroutine(Typewrite());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CallTypewrite(string toDisplay)
    {
        display = toDisplay;
        if (typewriteRoutine != null)
            StopCoroutine(typewriteRoutine);
        typewriteRoutine = StartCoroutine(Typewrite());
    }

    IEnumerator Typewrite()
    {
        int charsDrawn = 0;
        while(charsDrawn < display.Length)
        {
            charsDrawn++;

            string use = display.Substring(0, charsDrawn);
            textbox.text = use;
            yield return new WaitForSeconds(textSpeed);


        }
    }
}
