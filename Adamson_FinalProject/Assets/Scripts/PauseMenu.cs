﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public GameObject pauseCanvas;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("escape") && pauseCanvas.activeInHierarchy == false)
        {
            pauseCanvas.SetActive(true);
            Time.timeScale = 0.0f;
        }
        else
        if(Input.GetButtonDown("escape") && pauseCanvas.activeInHierarchy == true)
        {
            Resume();
        }
    }

    public void Resume()
    {
        pauseCanvas.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void ResetButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit()
    {
        Application.Quit();
    }


}
