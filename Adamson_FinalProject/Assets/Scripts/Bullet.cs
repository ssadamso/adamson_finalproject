﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject point;
    public Vector2 gravityDirection;
    public GunController gunScript;



    private void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        print("Ladies and gentlemen, we got her.");
        if (other.gameObject.layer == 8)
        {
            print("Ladies and gentlemen, we got him.");
            gravityDirection = point.transform.position - other.gameObject.transform.position;
            gravityDirection = gravityDirection.normalized;
            var rb2d = other.gameObject.GetComponent<Rigidbody2D>();
            rb2d.velocity = gravityDirection * 30;
            gunScript.isBullet = false;
            Destroy(gameObject);
        }

        if (other.gameObject.layer == 11)
        {
            gunScript.isBullet = false;
            Destroy(gameObject);
        }

        if (other.gameObject.layer == 12)
        {
            gunScript.isBullet = false;
            Destroy(gameObject);
        }
    }
}
