﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public Transform gunBarrel;
    public bool isWell;
    public bool isBullet;
    public GameObject well;
    public GameObject bullet;
    public GameObject reverseWell;
    public GameObject reverseBullet;
    public Vector2 fireDirection;
    public float gunType;

    GameObject wellInstance;
    //If 0, gravity well. If 1, bullet



    // Start is called before the first frame update
    void Start()
    {
        isWell = false;
        isBullet = false;
        gunType = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Switch2") && gunType == 0f)
        {
            gunType = 1f;
            print(gunType);
        }
        if (Input.GetButtonDown("Switch") && gunType == 1f)
        {
            gunType = 0f;
            print(gunType);
        }

            if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }

            if (Input.GetMouseButtonDown(1))
        {
            Shoot2();
        }

    }


    void Shoot()
    {
        if(isWell == false && gunType == 0f)
        {
            Vector2 mosPos = Input.mousePosition;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(mosPos.x, mosPos.y, 0f));
            worldPos = new Vector3(worldPos.x, worldPos.y, gunBarrel.position.z);
            fireDirection = transform.position - worldPos;
            fireDirection = fireDirection.normalized;
            if(wellInstance != null)
            {
                Destroy(wellInstance);
                isWell = false;
            }


            wellInstance = Instantiate(well, gunBarrel.position, gunBarrel.rotation);
            var rb2d = wellInstance.GetComponent<Rigidbody2D>();
            rb2d.velocity = fireDirection * -6f;
            isWell = true;
        }
        else
        if(isWell == true && gunType == 0f)
        {
            Destroy(wellInstance);
            isWell = false;
        }

        if(isBullet == false && gunType == 1f)
        {
            Vector2 mosPos = Input.mousePosition;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(mosPos.x, mosPos.y, 0f));
            worldPos = new Vector3(worldPos.x, worldPos.y, 0f);
            fireDirection = transform.position - worldPos;
            fireDirection = fireDirection.normalized;

            GameObject bInstance = Instantiate(bullet, gunBarrel.position, gunBarrel.rotation);
            var rb2d = bInstance.GetComponent<Rigidbody2D>();
            rb2d.velocity = fireDirection * -30f;
            isBullet = true;

            bInstance.GetComponent<Bullet>().gunScript = this;
        }

    }
    
    void Shoot2()
    {
        if (isWell == false && gunType == 0f)
        {
            Vector2 mosPos = Input.mousePosition;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(mosPos.x, mosPos.y, 0f));
            worldPos = new Vector3(worldPos.x, worldPos.y, gunBarrel.position.z);
            fireDirection = transform.position - worldPos;
            fireDirection = fireDirection.normalized;
            if (wellInstance != null)
            {
                Destroy(wellInstance);
                isWell = false;
            }


            wellInstance = Instantiate(reverseWell, gunBarrel.position, gunBarrel.rotation);
            var rb2d = wellInstance.GetComponent<Rigidbody2D>();
            rb2d.velocity = fireDirection * -6f;
            isWell = true;
        }
        else
       if (isWell == true && gunType == 0f)
        {
            Destroy(wellInstance);
            isWell = false;
        }

        if (isBullet == false && gunType == 1f)
        {
            Vector2 mosPos = Input.mousePosition;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(new Vector3(mosPos.x, mosPos.y, 0f));
            worldPos = new Vector3(worldPos.x, worldPos.y, 0f);
            fireDirection = transform.position - worldPos;
            fireDirection = fireDirection.normalized;

            GameObject bInstance = Instantiate(reverseBullet, gunBarrel.position, gunBarrel.rotation);
            var rb2d = bInstance.GetComponent<Rigidbody2D>();
            rb2d.velocity = fireDirection * -30f;
            isBullet = true;

            bInstance.GetComponent<ReverseBullet>().gunScript = this;
        }

    }
}
