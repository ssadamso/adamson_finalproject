﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReverseWell : MonoBehaviour
{
    public Vector2 gravityDirection;
    public GameObject hitBox;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonUp(1))
        {
            var selfrb2d = GetComponent<Rigidbody2D>();
            selfrb2d.Sleep();
            hitBox.SetActive(false);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.layer == 8)
        {
            gravityDirection = transform.position - other.gameObject.transform.position;
            gravityDirection = gravityDirection.normalized;
            gravityDirection = gravityDirection * -1;
            var rb2d = other.gameObject.GetComponent<Rigidbody2D>();
            rb2d.velocity = gravityDirection;

        }


    }
}
